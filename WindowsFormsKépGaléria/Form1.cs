﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsKépGaléria
{
    public partial class Form1 : Form
    {
        protected int aktuális;
        public Form1()
        {
            InitializeComponent();
            aktuális = 2;
        }

        private void btnVissza_Click(object sender, EventArgs e)
        {
            aktuális--;
            aktuális = aktuális < 1 ? 6 : aktuális;
            pBBal.ImageLocation = System.IO.Directory.GetCurrentDirectory() + "/Képek/" + (aktuális - 1 < 1 ? 6 : aktuális - 1).ToString() + ".jpeg";
            pBKözépső.ImageLocation = System.IO.Directory.GetCurrentDirectory() + "/Képek/" + (aktuális).ToString() + ".jpeg";
            pBJobb.ImageLocation = System.IO.Directory.GetCurrentDirectory() + "/Képek/" + (aktuális + 1 > 6 ? 1 : aktuális + 1).ToString() + ".jpeg";
        }

        private void btnTovább_Click(object sender, EventArgs e)
        {
            aktuális++;
            aktuális = aktuális >6 ? 1 : aktuális;
            pBBal.ImageLocation = System.IO.Directory.GetCurrentDirectory()+"/Képek/" + (aktuális - 1 < 1 ? 6 : aktuális - 1).ToString() + ".jpeg";
            pBKözépső.ImageLocation = System.IO.Directory.GetCurrentDirectory()+"/Képek/" + (aktuális).ToString() + ".jpeg";
            pBJobb.ImageLocation = System.IO.Directory.GetCurrentDirectory()+"/Képek/" + (aktuális + 1 > 6 ? 1 : aktuális + 1).ToString() + ".jpeg";

        }
    }
}
