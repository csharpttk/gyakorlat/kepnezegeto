﻿namespace WindowsFormsKépGaléria
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.label1 = new System.Windows.Forms.Label();
            this.pBBal = new System.Windows.Forms.PictureBox();
            this.pBKözépső = new System.Windows.Forms.PictureBox();
            this.pBJobb = new System.Windows.Forms.PictureBox();
            this.btnVissza = new System.Windows.Forms.Button();
            this.btnElőre = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.pBBal)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pBKözépső)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pBJobb)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(209, 20);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(143, 31);
            this.label1.TabIndex = 0;
            this.label1.Text = "Képgaléria";
            // 
            // pBBal
            // 
            this.pBBal.Image = ((System.Drawing.Image)(resources.GetObject("pBBal.Image")));
            this.pBBal.Location = new System.Drawing.Point(23, 83);
            this.pBBal.Name = "pBBal";
            this.pBBal.Size = new System.Drawing.Size(134, 79);
            this.pBBal.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pBBal.TabIndex = 1;
            this.pBBal.TabStop = false;
            // 
            // pBKözépső
            // 
            this.pBKözépső.Image = ((System.Drawing.Image)(resources.GetObject("pBKözépső.Image")));
            this.pBKözépső.Location = new System.Drawing.Point(163, 69);
            this.pBKözépső.Name = "pBKözépső";
            this.pBKözépső.Size = new System.Drawing.Size(189, 119);
            this.pBKözépső.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pBKözépső.TabIndex = 2;
            this.pBKözépső.TabStop = false;
            // 
            // pBJobb
            // 
            this.pBJobb.Image = ((System.Drawing.Image)(resources.GetObject("pBJobb.Image")));
            this.pBJobb.Location = new System.Drawing.Point(358, 83);
            this.pBJobb.Name = "pBJobb";
            this.pBJobb.Size = new System.Drawing.Size(142, 79);
            this.pBJobb.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pBJobb.TabIndex = 3;
            this.pBJobb.TabStop = false;
            // 
            // btnVissza
            // 
            this.btnVissza.Location = new System.Drawing.Point(38, 207);
            this.btnVissza.Name = "btnVissza";
            this.btnVissza.Size = new System.Drawing.Size(75, 23);
            this.btnVissza.TabIndex = 4;
            this.btnVissza.Text = "Vissza";
            this.btnVissza.UseVisualStyleBackColor = true;
            this.btnVissza.Click += new System.EventHandler(this.btnVissza_Click);
            // 
            // btnElőre
            // 
            this.btnElőre.Location = new System.Drawing.Point(424, 206);
            this.btnElőre.Name = "btnElőre";
            this.btnElőre.Size = new System.Drawing.Size(75, 23);
            this.btnElőre.TabIndex = 5;
            this.btnElőre.Text = "Tovább";
            this.btnElőre.UseVisualStyleBackColor = true;
            this.btnElőre.Click += new System.EventHandler(this.btnTovább_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(541, 261);
            this.Controls.Add(this.btnElőre);
            this.Controls.Add(this.btnVissza);
            this.Controls.Add(this.pBJobb);
            this.Controls.Add(this.pBKözépső);
            this.Controls.Add(this.pBBal);
            this.Controls.Add(this.label1);
            this.Name = "Form1";
            this.Text = "Form1";
            ((System.ComponentModel.ISupportInitialize)(this.pBBal)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pBKözépső)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pBJobb)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.PictureBox pBBal;
        private System.Windows.Forms.PictureBox pBKözépső;
        private System.Windows.Forms.PictureBox pBJobb;
        private System.Windows.Forms.Button btnVissza;
        private System.Windows.Forms.Button btnElőre;
    }
}

