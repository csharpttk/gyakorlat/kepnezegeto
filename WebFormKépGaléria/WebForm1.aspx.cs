﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WebFormKépGaléria
{
    public partial class WebForm1 : System.Web.UI.Page
    {
        private static int aktuális=2;
        protected void Page_Load(object sender, EventArgs e)
        {
            
        }

        protected void btnTovább_Click(object sender, EventArgs e)
        {
            aktuális++;
            aktuális = aktuális > 6 ? 1 : aktuális;
            imgBal.ImageUrl = "Képek/" + (aktuális - 1 < 1 ? 6 : aktuális - 1).ToString() + ".jpeg";
            imgKözépső.ImageUrl = "Képek/" + (aktuális).ToString() + ".jpeg";
            imgJobb.ImageUrl = "Képek/" + (aktuális + 1 > 6 ? 1 : aktuális + 1).ToString() + ".jpeg";

        }

        protected void btnVissza_Click(object sender, EventArgs e)
        {
            aktuális--;
            aktuális = aktuális < 1 ? 6 : aktuális;
            imgBal.ImageUrl = "Képek/" + (aktuális - 1 < 1 ? 6 : aktuális - 1).ToString() + ".jpeg";
            imgKözépső.ImageUrl =  "Képek/" + (aktuális).ToString() + ".jpeg";
            imgJobb.ImageUrl =  "Képek/" + (aktuális + 1 > 6 ? 1 : aktuális + 1).ToString() + ".jpeg";

        }
    }
}