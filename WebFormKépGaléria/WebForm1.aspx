﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="WebForm1.aspx.cs" Inherits="WebFormKépGaléria.WebForm1" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div>
         <asp:Table ID="Table1" runat="server">
              <asp:TableRow runat="server">
                   <asp:TableCell runat="server" columnspan="3">
                    <div style="margin-left: auto; margin-right: auto; text-align: center;">
                          <asp:Label ID="Label1" runat="server" Text="Képgaléria" Font-Size="X-Large"  CssClass="StrongText"></asp:Label>
                    </div>
                       </asp:TableCell>
              </asp:TableRow>
        
                <asp:TableRow runat="server">
                    <asp:TableCell runat="server" >
                        <asp:Image ID="imgBal" runat="server" style="width:200px" ImageUrl="~/Képek/1.jpeg" />
                    </asp:TableCell>
                    <asp:TableCell runat="server" >
                        <asp:Image ID="imgKözépső" runat="server" style="width:300px"  ImageUrl="~/Képek/2.jpeg"/>
                    </asp:TableCell>
                    <asp:TableCell runat="server" >
                        <asp:Image ID="imgJobb" runat="server" style="width:200px"   ImageUrl="~/Képek/3.jpeg"/>
                    </asp:TableCell>
                </asp:TableRow>
                  <asp:TableRow runat="server">
                       <asp:TableCell runat="server" >
                           <asp:Button ID="btnVissza" runat="server" Text="Vissza" OnClick="btnVissza_Click" />
                       </asp:TableCell>
                        <asp:TableCell runat="server" >                          
                       </asp:TableCell>
                      <asp:TableCell runat="server"  >
                           <asp:Button ID="btnTovább" style="position:relative; float:right;" runat="server" Text="Tovább"  OnClick="btnTovább_Click"/>
                       </asp:TableCell>
                  </asp:TableRow>
            </asp:Table>
        </div>
    </form>
</body>
</html>
