﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace WpfBrowserAppKépGaléria
{
    /// <summary>
    /// Interaction logic for Page1.xaml
    /// </summary>
    public partial class Page1 : Page
    {
        private int aktuális;
        public Page1()
        {
            InitializeComponent();
            aktuális = 2;
        }

        private void btnElőre_Click(object sender, RoutedEventArgs e)
        {
            aktuális--;
            aktuális = aktuális < 1 ? 6 : aktuális;
            imgBal.Source = new BitmapImage(new Uri("/Képek/" + (aktuális - 1 < 1 ? 6 : aktuális - 1).ToString() + ".jpeg", UriKind.Relative));
            imgKözépső.Source = new BitmapImage(new Uri("/Képek/" + (aktuális).ToString() + ".jpeg", UriKind.Relative));
            imgJobb.Source = new BitmapImage(new Uri("/Képek/" + (aktuális + 1 > 6 ? 1 : aktuális + 1).ToString() + ".jpeg", UriKind.Relative));
        }

        private void btnHátra_Click(object sender, RoutedEventArgs e)
        {           
            aktuális++;
            aktuális = aktuális > 6 ? 1 : aktuális;
            imgBal.Source = new BitmapImage(new Uri("/Képek/" + (aktuális - 1 < 1 ? 6 : aktuális - 1).ToString() + ".jpeg", UriKind.Relative));
            imgKözépső.Source = new BitmapImage(new Uri("/Képek/" + (aktuális).ToString() + ".jpeg", UriKind.Relative));
            imgJobb.Source = new BitmapImage(new Uri("/Képek/" + (aktuális + 1 > 6 ? 1 : aktuális + 1).ToString() + ".jpeg", UriKind.Relative));
        }
    }
}
